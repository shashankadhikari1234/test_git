package org.example.rough;

import java.util.Random;

public class RandomNumber {
    public static void main(String[] args) {
        int numberOfCode = 20;
        for(int i =0; i<numberOfCode; i++){
            String s = generateRandomNumber();
            System.out.println("kr_"+s);
        }
    }
   public static String generateRandomNumber() {
       String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
       StringBuilder code = new StringBuilder();
       for(int i = 0; i < 50; i++) {
           int index = (int) (Math.random() * characters.length());
           code.append(characters.charAt(index));
       }
       return code.toString();
   }
}
