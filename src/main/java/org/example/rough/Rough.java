package org.example.rough;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Rough {
    public static void main(String[] args) {
      LocalDate localDate = LocalDate.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");

        String format = formatter.format(localDate);
        System.out.println(format);

    }
}
